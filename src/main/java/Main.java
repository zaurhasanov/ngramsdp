import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.NgramUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    static Logger logger = LoggerFactory.getLogger(Main.class);
    static String fileName = "/home/user1/news.txt";
    static File file = new File(fileName);
    static FileReader fr;

    static {
        try {
            fr = new FileReader(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static BufferedReader br = new BufferedReader(fr);
    static String line;


    public static void main(String[] args) throws Exception {
        System.out.println("App Stared");
        //Reading txt file into string
        String data = new String(Files.readAllBytes(Paths.get(fileName)),"UTF-8");
        System.out.println("Loaded data to String");
        //Start to generate Ngram and add to db
//        String data="Salam test bu@#di bele 1dsad mada?? Salam, Eleyum]} /.  00Azerbaycan(){}";
        NgramUtil.generateNgrams(data);
    }

}
