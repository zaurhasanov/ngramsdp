package util;

import ngrams.NgramCore;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public class DatabaseUtil {


    public static String addUnigram(ArrayList<String> words) {
        ArrayList<String> ngrams = NgramCore.generateNgrams(words, 1);
        try (Connection connection = DataSource.getConnection()) {
            CallableStatement callableStatement = connection.prepareCall("call ngram.insertToUnigram(?)");
            for (String gram : ngrams) {
                callableStatement.setString(1, gram);
                callableStatement.addBatch();
            }
            callableStatement.executeBatch();
            connection.commit();
            if (callableStatement != null)
                callableStatement.close();
            if (connection != null)
                connection.close();
            return "OK";
        } catch (SQLException e) {
            e.printStackTrace();
            return "FAIL";
        }
    }


    public static String addBigram(ArrayList<String> words) {
        ArrayList<String> ngrams = NgramCore.generateNgrams(words, 2);
        try (Connection connection = DataSource.getConnection()) {
            CallableStatement callableStatement = connection.prepareCall("call ngram.insertToBigram(?,?);");
            for (String gram : ngrams) {
                String grams[] = gram.trim().split("\\s+");
                callableStatement.setString(1, grams[0]);
                callableStatement.setString(2, grams[1]);
                callableStatement.addBatch();
            }
            callableStatement.executeBatch();
            connection.commit();
            if (callableStatement != null)
                callableStatement.close();
            if (connection != null)
                connection.close();
            return "OK";
        } catch (SQLException e) {
            e.printStackTrace();
            return "FAIL";
        }

    }

    public static String addThreegram(ArrayList<String> words) {
        String procedure = "call ngram.insertToThreegram(?,?,?)";
        ArrayList<String> ngrams = NgramCore.generateNgrams(words, 3);
        try (Connection connection = DataSource.getConnection()) {
            CallableStatement callableStatement = connection.prepareCall(procedure);
            for (String gram : ngrams) {
                String grams[] = gram.trim().split("\\s+");
                callableStatement.setString(1, grams[0]);
                callableStatement.setString(2, grams[1]);
                callableStatement.setString(3, grams[2]);
                callableStatement.addBatch();
            }
            callableStatement.executeBatch();
            connection.commit();
            if (callableStatement != null)
                callableStatement.close();
            if (connection != null)
                connection.close();
            return "OK";
        } catch (SQLException e) {
            e.printStackTrace();
            return "FAIL";
        }
    }


    public static String addFourgram(ArrayList<String> words) {
        String procedure = "call ngram.insertToFourgram(?,?,?,?)";
        ArrayList<String> ngrams = NgramCore.generateNgrams(words, 4);
        try (Connection connection = DataSource.getConnection()) {
            CallableStatement callableStatement = connection.prepareCall(procedure);
            for (String gram : ngrams) {
                String grams[] = gram.trim().split("\\s+");
                callableStatement.setString(1, grams[0]);
                callableStatement.setString(2, grams[1]);
                callableStatement.setString(3, grams[2]);
                callableStatement.setString(4, grams[3]);
                callableStatement.addBatch();
            }
            callableStatement.executeBatch();
            connection.commit();
            if (callableStatement != null)
                callableStatement.close();
            if (connection != null)
                connection.close();
            return "OK";
        } catch (SQLException e) {
            e.printStackTrace();
            return "FAIL";
        }
    }


    public static String addFivegram(ArrayList<String> words) {
        String procedure = "call ngram.insertToFivegram(?,?,?,?,?)";
        ArrayList<String> ngrams = NgramCore.generateNgrams(words, 5);
        try (Connection connection = DataSource.getConnection()) {
            CallableStatement callableStatement = connection.prepareCall(procedure);
            for (String gram : ngrams) {
                String grams[] = gram.trim().split("\\s+");
                callableStatement.setString(1, grams[0]);
                callableStatement.setString(2, grams[1]);
                callableStatement.setString(3, grams[2]);
                callableStatement.setString(4, grams[3]);
                callableStatement.setString(5, grams[4]);
                callableStatement.addBatch();
            }
            callableStatement.executeBatch();
            connection.commit();
            if (callableStatement != null)
                callableStatement.close();
            if (connection != null)
                connection.close();
            return "OK";
        } catch (SQLException e) {
            e.printStackTrace();
            return "FAIL";
        }
    }

}
