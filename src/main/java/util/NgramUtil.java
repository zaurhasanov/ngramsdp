package util;

import ngrams.NgramCore;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class NgramUtil {

    static Executor executor = Executors.newFixedThreadPool(6);

    public static void generateNgrams(String line) {
        ArrayList<String> words;
        try {
            //separating into words
            words = NgramCore.separateToWords(line);
            line=null;
        } catch (Exception e) {
            return;
        }
        //Started  5 thread  for creating ngrams and inserting db
        CompletableFuture<String> futureUnigram = CompletableFuture.supplyAsync(() -> {

            DatabaseUtil.addUnigram(words);
            return "OK";
        }, executor);

        CompletableFuture<String> futureBigram = CompletableFuture.supplyAsync(() -> {
            DatabaseUtil.addBigram(words);
            return "OK";
        }, executor);

        CompletableFuture<String> futurThreegram = CompletableFuture.supplyAsync(() -> {
            DatabaseUtil.addThreegram(words);
            return "OK";
        }, executor);

        CompletableFuture<String> futurFourgram = CompletableFuture.supplyAsync(() -> {
            DatabaseUtil.addFourgram(words);
            return "OK";
        }, executor);
        CompletableFuture<String> futurFivegram = CompletableFuture.supplyAsync(() -> {
            DatabaseUtil.addFivegram(words);
            return "OK";
        }, executor);

        try {
            System.out.println("OK".equals(futureUnigram.get()) + ":UniGram");
            System.out.println("OK".equals(futureBigram.get()) + ":BiGram");
            System.out.println("OK".equals(futurThreegram.get()) + ":ThreeGram");
            System.out.println("OK".equals(futurFourgram.get()) + ":FourGram");
            System.out.println("OK".equals(futurFivegram.get()) + ":FiveGram");

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return;
    }

}
